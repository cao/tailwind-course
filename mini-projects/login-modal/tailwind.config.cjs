/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/*.{html,js}", "./src/**/*.{html,js}"],
  theme: {
    fontFamily: {
      sans: ["Mulish", "sans-serif"],
      mono: ["Rokkitt", "monospace"],
    },
    extend: {},
  },
  plugins: [],
};
