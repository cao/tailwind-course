import "./style.css";

const linkInput = document.getElementById("link-input");
const errMsg = document.getElementById("err-msg");
const form = document.getElementById("link-form");

linkInput.addEventListener("keyup", (e) => {
  if (e.code === "Enter") {
  }
  valid();
});
form.addEventListener("submit", (e) => {
  e.preventDefault();
  valid();
});

function isValidUrl(url) {
  return /^http(s?)\:\/\/.+?$/.test(url.trim());
}

function valid() {
  if (!isValidUrl(linkInput.value)) {
    linkInput.classList.add("border-red");
    errMsg.innerHTML = "请输入正确的网址";
  } else {
    linkInput.classList.remove("border-red");
    errMsg.innerHTML = "";
  }
}

const menuBtn = document.getElementById("menu-btn");
const menu = document.getElementById("menu");
menuBtn.addEventListener("click", () => {
  menuBtn.classList.toggle("open");
  menu.classList.toggle("hidden");
});
