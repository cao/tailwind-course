import "./style.css";

const menuBtn = document.getElementById("menu-btn");
const menu = document.getElementById("menu");
const logo = document.getElementById("logo");

menuBtn.addEventListener("click", () => {
  menuBtn.classList.toggle("open");
  menu.classList.toggle("hidden");
  menu.classList.toggle("flex");
  const logoFile = menuBtn.classList.contains("open")
    ? "/images/logo-bookmark-footer.svg"
    : "images/logo-bookmark.svg";
  logo.setAttribute("src", logoFile);
});

const tabs = document.querySelectorAll(".tab");
const panels = document.querySelectorAll(".panel");
const activeTabClassList = ["border-softRed", "border-b-4"];

tabs.forEach((tab) => {
  tab.addEventListener("click", (e) => {
    // console.log(e.target);
    // 取消选中
    tabs.forEach((tab) => {
      tab.children[0].classList.remove(...activeTabClassList);
    });
    // 隐藏所有面板
    panels.forEach((panel) => {
      panel.classList.add("hidden");
      panel.classList.remove("flex");
    });

    // 当前选项
    e.target.classList.add(...activeTabClassList);
    const activePanelName = e.target.getAttribute("data-target") || "panel-1";
    const activePanel = document.querySelector(`.${activePanelName}`);
    activePanel.classList.add("flex");
    activePanel.classList.remove("hidden");
  });
});
